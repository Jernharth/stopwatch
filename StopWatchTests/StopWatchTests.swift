//
//  StopWatchTests.swift
//  StopWatchTests
//
//  Created by Jacob Bernharth on 2020-05-21.
//  Copyright © 2020 Jernharth. All rights reserved.
//

import XCTest
@testable import StopWatch

class StopWatchTests: XCTestCase {
    
    private let dm = DummyStopWatchDataModel()
    
    func testStopWatch() {
        let vm = StopWatchViewModel(dataModel: dm)
        dm.elapsedTime = 0
        XCTAssertEqual(vm.isStopWatchRunning, false)
        
        // Note: The currently used CADisplayLink has after further testing proven not accurate down to 10ms. (0.01 increase every 2 seconds). I will therefore not test this further as I would find a more accurate method instead.
        let ex = expectation(description: "Elapsed time should be accurate up to 0.1 seconds")

        vm.startReceivingTime { time in
            XCTAssertEqual(vm.isStopWatchRunning, true)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
            vm.pauseReceivingTime()
            XCTAssertEqual(vm.isStopWatchRunning, false)
            ex.fulfill()
        })
        
        wait(for: [ex], timeout: 2.5)
        
        vm.stopReceivingTime()
        XCTAssertEqual(vm.isStopWatchRunning, false)
        XCTAssertEqual(dm.elapsedTime, 0)
        // Note: Strangely run 122 times for 2 seconds. Maybe 61 FPS? Seems connected to note above.
        XCTAssertEqual(dm.updateElapsedTimeCalls, 122)
    }
    
    func testIsElapsedValueZero() {
        let vm = StopWatchViewModel(dataModel: dm)
        dm.elapsedTime = 10
        XCTAssertEqual(vm.isElapsedValueZero(), false)
        dm.elapsedTime = 0
        XCTAssertEqual(vm.isElapsedValueZero(), true)
    }
    
    func testGetFormattedElapsedTime() {
        let vm = StopWatchViewModel(dataModel: dm)
        dm.elapsedTime = 0
        XCTAssertEqual(vm.getFormattedElapsedTime(), "00:00:00.00")
        dm.elapsedTime = 0.01
        XCTAssertEqual(vm.getFormattedElapsedTime(), "00:00:00.01")
        dm.elapsedTime = 0.1
        XCTAssertEqual(vm.getFormattedElapsedTime(), "00:00:00.10")
        dm.elapsedTime = 1
        XCTAssertEqual(vm.getFormattedElapsedTime(), "00:00:01.00")
        dm.elapsedTime = 10
        XCTAssertEqual(vm.getFormattedElapsedTime(), "00:00:10.00")
        dm.elapsedTime = 60
        XCTAssertEqual(vm.getFormattedElapsedTime(), "00:01:00.00")
        dm.elapsedTime = 600
        XCTAssertEqual(vm.getFormattedElapsedTime(), "00:10:00.00")
        dm.elapsedTime = 3600
        XCTAssertEqual(vm.getFormattedElapsedTime(), "01:00:00.00")
        dm.elapsedTime = 36000
        XCTAssertEqual(vm.getFormattedElapsedTime(), "10:00:00.00")
    }
    
    func testResetStopWatch() {
        let vm = StopWatchViewModel(dataModel: dm)
        vm.resetStopWatch()
        XCTAssertEqual(dm.resetDataCalls, 1)
    }
    
    func testStoreCurrentData() {
        let vm = StopWatchViewModel(dataModel: dm)
        vm.storeCurrentData()
        XCTAssertEqual(dm.storeCurrentDataCalls, 1)
    }

    func testCreateLap() {
        let vm = StopWatchViewModel(dataModel: dm)
        dm.elapsedTime = 0
        vm.createLap()
        XCTAssertEqual(dm.laps.last?.time, "00:00:00.00")
        XCTAssertEqual(dm.laps.last?.difference, nil)
        dm.elapsedTime = 0.01
        vm.createLap()
        XCTAssertEqual(dm.laps.last?.time, "00:00:00.01")
        XCTAssertEqual(dm.laps.last?.difference, "00:00:00.01")
        dm.elapsedTime = 0.1
        vm.createLap()
        XCTAssertEqual(dm.laps.last?.time, "00:00:00.10")
        XCTAssertEqual(dm.laps.last?.difference, "00:00:00.09")
        dm.elapsedTime = 1
        vm.createLap()
        XCTAssertEqual(dm.laps.last?.time, "00:00:01.00")
        XCTAssertEqual(dm.laps.last?.difference, "00:00:00.90")
        dm.elapsedTime = 10
        vm.createLap()
        XCTAssertEqual(dm.laps.last?.time, "00:00:10.00")
        XCTAssertEqual(dm.laps.last?.difference, "00:00:09.00")
        dm.elapsedTime = 100
        vm.createLap()
        XCTAssertEqual(dm.laps.last?.time, "00:01:40.00")
        XCTAssertEqual(dm.laps.last?.difference, "00:01:30.00")
        dm.elapsedTime = 1000
        vm.createLap()
        XCTAssertEqual(dm.laps.last?.time, "00:16:40.00")
        XCTAssertEqual(dm.laps.last?.difference, "00:15:00.00")
        dm.elapsedTime = 10000
        vm.createLap()
        XCTAssertEqual(dm.laps.last?.time, "02:46:40.00")
        XCTAssertEqual(dm.laps.last?.difference, "02:30:00.00")
        dm.elapsedTime = 100000
        vm.createLap()
        XCTAssertEqual(dm.laps.last?.time, "03:46:40.00")
        XCTAssertEqual(dm.laps.last?.difference, "01:00:00.00")
        dm.elapsedTime = 1000000
        vm.createLap()
        XCTAssertEqual(dm.laps.last?.time, "13:46:40.00")
        XCTAssertEqual(dm.laps.last?.difference, "10:00:00.00")
        dm.elapsedTime = 10000000
        vm.createLap()
        XCTAssertEqual(dm.laps.last?.time, "17:46:40.00")
        XCTAssertEqual(dm.laps.last?.difference, "04:00:00.00")
        dm.elapsedTime = 100000000
        vm.createLap()
        XCTAssertEqual(dm.laps.last?.time, "09:46:40.00")
        XCTAssertEqual(dm.laps.last?.difference, "16:00:00.00")
        
        XCTAssertEqual(dm.updateLapsCalls, 12)
    }
}

private class DummyStopWatchDataModel: StopWatchDataModelProtocol {
    
    private(set) var updateLapsCalls = 0
    private(set) var updateElapsedTimeCalls = 0
    private(set) var resetDataCalls = 0
    private(set) var storeCurrentDataCalls = 0
    
    var laps: [Lap] = []
    
    var elapsedTime: CFTimeInterval = 0
    
    func updateLaps(with lap: Lap) {
        updateLapsCalls += 1
        laps.append(lap)
    }
    
    func updateElapsedTime(with time: CFTimeInterval) {
        updateElapsedTimeCalls += 1
        elapsedTime += time
    }
    
    func resetData() {
        resetDataCalls += 1
    }
    
    func storeCurrentData() {
        storeCurrentDataCalls += 1
    }
}
