//
//  StopWatchUITests.swift
//  StopWatchUITests
//
//  Created by Jacob Bernharth on 2020-05-21.
//  Copyright © 2020 Jernharth. All rights reserved.
//

import XCTest

class StopWatchUITests: XCTestCase {

    func testLaunchPerformance() throws {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, *) {
            // This measures how long it takes to launch your application.
            measure(metrics: [XCTOSSignpostMetric.applicationLaunch]) {
                XCUIApplication().launch()
            }
        }
    }
}
