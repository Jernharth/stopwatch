//
//  StopWatchDataModel.swift
//  StopWatch
//
//  Created by Jacob Bernharth on 2020-06-01.
//  Copyright © 2020 Jernharth. All rights reserved.
//

import Foundation

protocol StopWatchDataModelProtocol: class {
    var laps: [Lap] { get }
    
    var elapsedTime: CFTimeInterval { get }
    
    func updateLaps(with lap: Lap)
    
    func updateElapsedTime(with time: CFTimeInterval)
    
    func resetData()
    
    func storeCurrentData()
}

final class StopWatchDataModel: StopWatchDataModelProtocol {
    private let dataStore: StopWatchDataStore
    
    private(set) var laps: [Lap]

    private(set) var elapsedTime: CFTimeInterval

    init(dataStore: StopWatchDataStore) {
        self.dataStore = dataStore
        
        let laps = dataStore.provideLaps()
        self.laps = laps
        
        let elapsedTime = dataStore.provideElapsedTime()
        self.elapsedTime = elapsedTime
    }
    
    func updateLaps(with lap: Lap) {
        laps.append(lap)
    }
    
    func updateElapsedTime(with time: CFTimeInterval) {
        elapsedTime += time
    }
    
    func resetData() {
        laps.removeAll()
        elapsedTime = 0
    }
    
    func storeCurrentData() {
        dataStore.update(laps: laps)
        dataStore.update(elapsedTime: elapsedTime)
    }
}
