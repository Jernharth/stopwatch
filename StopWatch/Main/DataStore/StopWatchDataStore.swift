//
//  StopWatchDataStore.swift
//  StopWatch
//
//  Created by Jacob Bernharth on 2020-05-29.
//  Copyright © 2020 Jernharth. All rights reserved.
//

import Foundation

import Foundation

final class StopWatchDataStore {
    private let userDefaults: UserDefaults = .standard
    private let lapsKey = "lap"
    private let elapsedTimeKey = "elapsedTime"
    
    func update(laps: [Lap]) {
        let lapsData = laps.map({ try? PropertyListEncoder().encode($0) })
        userDefaults.set(lapsData, forKey: lapsKey)
    }
    
    func update(elapsedTime: Double) {
        userDefaults.set(elapsedTime, forKey: elapsedTimeKey)
    }
    
    func provideLaps() -> [Lap] {
        guard let lapsData = userDefaults.array(forKey: lapsKey) as? [Data] else {
            return []
        }
        return lapsData.compactMap({ try? PropertyListDecoder().decode(Lap.self, from: $0)})
    }
    
    func provideElapsedTime() -> CFTimeInterval {
        return userDefaults.double(forKey: elapsedTimeKey)
    }
}
