//
//  Lap.swift
//  StopWatch
//
//  Created by Jacob Bernharth on 2020-05-27.
//  Copyright © 2020 Jernharth. All rights reserved.
//

import Foundation

struct Lap: Codable {
    let time: String
    let difference: String?
}
