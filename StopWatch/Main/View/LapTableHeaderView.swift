//
//  LapTableHeaderView.swift
//  StopWatch
//
//  Created by Jacob Bernharth on 2020-05-29.
//  Copyright © 2020 Jernharth. All rights reserved.
//

import UIKit

final class LapTableHeaderView: UITableViewHeaderFooterView {
    static let reuseIdentifier = String(describing: LapTableHeaderView.self)
    
    private let leftTitleLabel: UILabel
    private let rightTitleLabel: UILabel
    
    override init(reuseIdentifier: String?) {
        leftTitleLabel = UILabel()
        rightTitleLabel = UILabel()
        
        super.init(reuseIdentifier: reuseIdentifier)
        
        setupUI()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - View setup
extension LapTableHeaderView {
    private func setupUI() {
        leftTitleLabel.text = "Lap time"
        rightTitleLabel.text = "Difference"
        
        leftTitleLabel.textAlignment = .center
        rightTitleLabel.textAlignment = .center
        
        addSubview(leftTitleLabel)
        addSubview(rightTitleLabel)
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        leftTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        rightTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            leftTitleLabel.topAnchor.constraint(equalTo: topAnchor, constant: 15),
            leftTitleLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -15),
            leftTitleLabel.leftAnchor.constraint(equalTo: leftAnchor),
            leftTitleLabel.rightAnchor.constraint(equalTo: centerXAnchor),
            
            rightTitleLabel.topAnchor.constraint(equalTo: topAnchor, constant: 15),
            rightTitleLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -15),
            rightTitleLabel.leftAnchor.constraint(equalTo: centerXAnchor),
            rightTitleLabel.rightAnchor.constraint(equalTo: rightAnchor),
        ])
    }
}
