//
//  LapTableViewCell.swift
//  StopWatch
//
//  Created by Jacob Bernharth on 2020-05-28.
//  Copyright © 2020 Jernharth. All rights reserved.
//

import UIKit

final class LapTableViewCell: UITableViewCell {
    static let reuseIdentifier = String(describing: LapTableViewCell.self)
    
    private let timeLabel: UILabel
    private let differenceLabel: UILabel
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        timeLabel = UILabel()
        differenceLabel = UILabel()
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupUI()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configue(with lap: Lap) {
        timeLabel.text = lap.time
        differenceLabel.text = lap.difference
    }
}

// MARK: - View setup
extension LapTableViewCell {
    private func setupUI() {
        backgroundColor = .clear
        selectionStyle = .none
        
        timeLabel.textAlignment = .center
        differenceLabel.textAlignment = .center
        
        addSubview(timeLabel)
        addSubview(differenceLabel)
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        timeLabel.translatesAutoresizingMaskIntoConstraints = false
        differenceLabel.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            timeLabel.topAnchor.constraint(equalTo: topAnchor, constant: 15),
            timeLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -15),
            timeLabel.leftAnchor.constraint(equalTo: leftAnchor),
            timeLabel.rightAnchor.constraint(equalTo: centerXAnchor),
            
            differenceLabel.topAnchor.constraint(equalTo: topAnchor, constant: 15),
            differenceLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -15),
            differenceLabel.leftAnchor.constraint(equalTo: centerXAnchor),
            differenceLabel.rightAnchor.constraint(equalTo: rightAnchor),
        ])
    }
}
