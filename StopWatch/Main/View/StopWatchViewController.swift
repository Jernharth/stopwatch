//
//  StopWatchViewController.swift
//  StopWatch
//
//  Created by Jacob Bernharth on 2020-05-21.
//  Copyright © 2020 Jernharth. All rights reserved.
//

import UIKit

final class StopWatchViewController: UIViewController {
    private let viewModel: StopWatchViewModel
    
    private let timeLabel: UILabel
    
    private let timeContainerView: UIView
    private let lapContainerView: UIView
    private let buttonContainerView: UIView
    private let resetButtonContainerView: UIView
    private let lapButtonContainerView: UIView
    
    private let timeButton: UIButton
    private let resetButton: UIButton
    private let lapButton: UIButton
    
    private let lapTableView: UITableView
    
    private var compactConstraints: [NSLayoutConstraint] = []
    private var regularConstraints: [NSLayoutConstraint] = []
    private var sharedConstraints: [NSLayoutConstraint] = []
        
    init(viewModel: StopWatchViewModel) {
        self.viewModel = viewModel
        
        timeLabel = UILabel()
        
        timeContainerView = UIView()
        lapContainerView = UIView()
        buttonContainerView = UIView()
        resetButtonContainerView = UIView()
        lapButtonContainerView = UIView()
        
        timeButton = UIButton()
        resetButton = UIButton()
        lapButton = UIButton()
        
        lapTableView = UITableView()
        
        super.init(nibName: nil, bundle: nil)
        
        setupUI()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(applicationWillResignActive), name: UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(applicationWillTerminate), name: UIApplication.willTerminateNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(applicationWillEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
        
        lapTableView.dataSource = self
        lapTableView.delegate = self
        
        view.addSubview(timeContainerView)
        view.insertSubview(buttonContainerView, belowSubview: timeContainerView)
        view.insertSubview(lapContainerView, belowSubview: buttonContainerView)
        timeContainerView.addSubview(timeButton)
        timeContainerView.addSubview(timeLabel)
        lapContainerView.addSubview(lapTableView)
        buttonContainerView.addSubview(resetButtonContainerView)
        buttonContainerView.addSubview(lapButtonContainerView)
        resetButtonContainerView.addSubview(resetButton)
        lapButtonContainerView.addSubview(lapButton)
        
        setupConstraints()
        
        NSLayoutConstraint.activate(sharedConstraints)
        layoutTrait(traitCollection: UIScreen.main.traitCollection)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        updateLayers()
    }
    
    @objc private func applicationWillResignActive() {
        if viewModel.isStopWatchRunning {
            pulsingTime(enabled: true)
        }
        viewModel.pauseReceivingTime()
        viewModel.storeCurrentData()
    }
    
    @objc private func applicationDidBecomeActive() {
        // Start pulsing animation if it was active when exiting application
        if !viewModel.isElapsedValueZero() {
            pulsingTime(enabled: true)
        }
    }
    
    @objc private func applicationWillTerminate() {
        viewModel.storeCurrentData()
    }
    
    @objc private func applicationWillEnterForeground() {
        // Start pulsing animation if it was active when entering background
        if !viewModel.isElapsedValueZero() {
            pulsingTime(enabled: true)
        }
    }
}

// MARK: - Stop watch handling
extension StopWatchViewController {
    @objc private func didPressTimeButton() {
        if viewModel.isStopWatchRunning {
            viewModel.pauseReceivingTime()
            pulsingTime(enabled: true)
        }
        else {
            pulsingTime(enabled: false)
            viewModel.startReceivingTime { [weak self] timeString in
                self?.timeLabel.text = timeString
            }
        }
    }
    
    @objc private func didPressResetButton() {
        viewModel.resetStopWatch()
        viewModel.stopReceivingTime()
        timeLabel.text = viewModel.getFormattedElapsedTime()
        pulsingTime(enabled: false)
        lapTableView.reloadData()
    }
    
    @objc private func didPressLapButton() {
        viewModel.createLap()
        lapTableView.reloadData()
    }
    
    private func pulsingTime(enabled: Bool) {
        if enabled {
            let pulseAnimation = CABasicAnimation(keyPath: "opacity")
            pulseAnimation.duration = 0.6
            pulseAnimation.fromValue = 1
            pulseAnimation.toValue = 0
            pulseAnimation.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
            pulseAnimation.autoreverses = true
            pulseAnimation.repeatCount = .greatestFiniteMagnitude
            timeLabel.layer.add(pulseAnimation, forKey: nil)
        }
        else {
            timeLabel.layer.removeAllAnimations()
        }
    }
}

// MARK: - View setup
extension StopWatchViewController {
    private func setupUI() {
        view.backgroundColor = .systemBackground
        
        timeLabel.text = viewModel.getFormattedElapsedTime()
        timeLabel.textAlignment = .center
        timeLabel.textColor = .label
        timeLabel.font = .monospacedDigitSystemFont(ofSize: 100, weight: .bold)
        timeLabel.adjustsFontSizeToFitWidth = true
        timeLabel.numberOfLines = 1
        timeLabel.minimumScaleFactor = 0.1
        timeLabel.baselineAdjustment = .alignCenters
        
        resetButtonContainerView.backgroundColor = .clear
        lapButtonContainerView.backgroundColor = .clear
        
        lapTableView.separatorColor = .clear
        lapTableView.rowHeight = UITableView.automaticDimension
        lapTableView.sectionHeaderHeight = UITableView.automaticDimension
        lapTableView.estimatedRowHeight = 50
        lapTableView.estimatedSectionHeaderHeight = 50
        lapTableView.register(LapTableViewCell.self, forCellReuseIdentifier: LapTableViewCell.reuseIdentifier)
        lapTableView.register(LapTableHeaderView.self, forHeaderFooterViewReuseIdentifier: LapTableHeaderView.reuseIdentifier)
        
        timeButton.setTitle("", for: .normal)
        timeButton.addTarget(self, action: #selector(didPressTimeButton), for: .touchUpInside)
        
        resetButton.setTitle("Reset", for: .normal)
        resetButton.setTitleColor(.label, for: .normal)
        resetButton.titleLabel?.font = .systemFont(ofSize: 18)
        resetButton.titleLabel?.adjustsFontSizeToFitWidth = true
        resetButton.titleLabel?.numberOfLines = 1
        resetButton.titleLabel?.minimumScaleFactor = 0.1
        resetButton.backgroundColor = .systemBackground
        resetButton.addTarget(self, action: #selector(didPressResetButton), for: .touchUpInside)
        
        lapButton.setTitle("Lap", for: .normal)
        lapButton.setTitleColor(.label, for: .normal)
        lapButton.titleLabel?.font = .systemFont(ofSize: 18)
        lapButton.titleLabel?.adjustsFontSizeToFitWidth = true
        lapButton.titleLabel?.numberOfLines = 1
        lapButton.titleLabel?.minimumScaleFactor = 0.1
        lapButton.backgroundColor = .systemBackground
        lapButton.addTarget(self, action: #selector(didPressLapButton), for: .touchUpInside)
    }
    
    private func updateLayers() {
        timeButton.layer.borderWidth = 2
        timeButton.layer.borderColor = UIColor.label.cgColor
        timeButton.layer.cornerRadius = timeButton.bounds.size.width / 2
        
        resetButton.layer.borderWidth = 2
        resetButton.layer.borderColor = UIColor.label.cgColor
        resetButton.layer.cornerRadius = resetButton.bounds.size.width / 4
        
        lapButton.layer.borderWidth = 2
        lapButton.layer.borderColor = UIColor.label.cgColor
        lapButton.layer.cornerRadius = lapButton.bounds.size.width / 4
    }
    
    private func setupConstraints() {
        timeLabel.translatesAutoresizingMaskIntoConstraints = false
        timeContainerView.translatesAutoresizingMaskIntoConstraints = false
        lapContainerView.translatesAutoresizingMaskIntoConstraints = false
        buttonContainerView.translatesAutoresizingMaskIntoConstraints = false
        timeButton.translatesAutoresizingMaskIntoConstraints = false
        resetButton.translatesAutoresizingMaskIntoConstraints = false
        lapButton.translatesAutoresizingMaskIntoConstraints = false
        lapTableView.translatesAutoresizingMaskIntoConstraints = false
        resetButtonContainerView.translatesAutoresizingMaskIntoConstraints = false
        lapButtonContainerView.translatesAutoresizingMaskIntoConstraints = false
        
        compactConstraints = [
            timeContainerView.topAnchor.constraint(equalTo: view.topAnchor),
            timeContainerView.bottomAnchor.constraint(equalTo: buttonContainerView.topAnchor),
            timeContainerView.leftAnchor.constraint(equalTo: view.leftAnchor),
            timeContainerView.rightAnchor.constraint(equalTo: view.rightAnchor),

            lapContainerView.topAnchor.constraint(equalTo: buttonContainerView.bottomAnchor),
            lapContainerView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            lapContainerView.leftAnchor.constraint(equalTo: view.leftAnchor),
            lapContainerView.rightAnchor.constraint(equalTo: view.rightAnchor),

            buttonContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            buttonContainerView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            buttonContainerView.widthAnchor.constraint(equalTo: view.widthAnchor),
            buttonContainerView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.1),
            
            lapButtonContainerView.topAnchor.constraint(equalTo: buttonContainerView.topAnchor),
            lapButtonContainerView.bottomAnchor.constraint(equalTo: buttonContainerView.bottomAnchor),
            lapButtonContainerView.leftAnchor.constraint(equalTo: buttonContainerView.centerXAnchor),
            lapButtonContainerView.rightAnchor.constraint(equalTo: buttonContainerView.rightAnchor),
            
            resetButtonContainerView.topAnchor.constraint(equalTo: buttonContainerView.topAnchor),
            resetButtonContainerView.bottomAnchor.constraint(equalTo: buttonContainerView.bottomAnchor),
            resetButtonContainerView.leftAnchor.constraint(equalTo: buttonContainerView.leftAnchor),
            resetButtonContainerView.rightAnchor.constraint(equalTo: buttonContainerView.centerXAnchor),
            
            timeButton.centerXAnchor.constraint(equalTo: timeContainerView.centerXAnchor),
            timeButton.centerYAnchor.constraint(equalTo: timeContainerView.centerYAnchor),
            timeButton.widthAnchor.constraint(equalTo: timeButton.heightAnchor),
            timeButton.heightAnchor.constraint(equalTo: timeContainerView.heightAnchor, multiplier: 0.75),
            
            lapButton.centerXAnchor.constraint(equalTo: lapButtonContainerView.centerXAnchor),
            lapButton.centerYAnchor.constraint(equalTo: lapButtonContainerView.centerYAnchor),
            lapButton.widthAnchor.constraint(equalTo: lapButtonContainerView.widthAnchor, multiplier: 0.6),
            lapButton.heightAnchor.constraint(equalTo: lapButtonContainerView.heightAnchor, multiplier: 0.7),
            
            resetButton.centerXAnchor.constraint(equalTo: resetButtonContainerView.centerXAnchor),
            resetButton.centerYAnchor.constraint(equalTo: resetButtonContainerView.centerYAnchor),
            resetButton.widthAnchor.constraint(equalTo: resetButtonContainerView.widthAnchor, multiplier: 0.6),
            resetButton.heightAnchor.constraint(equalTo: resetButtonContainerView.heightAnchor, multiplier: 0.7),
        ]
        
        regularConstraints = [
            timeContainerView.topAnchor.constraint(equalTo: view.topAnchor),
            timeContainerView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            timeContainerView.leftAnchor.constraint(equalTo: buttonContainerView.rightAnchor),
            timeContainerView.rightAnchor.constraint(equalTo: view.rightAnchor),

            lapContainerView.topAnchor.constraint(equalTo: view.topAnchor),
            lapContainerView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            lapContainerView.leftAnchor.constraint(equalTo: view.leftAnchor),
            lapContainerView.rightAnchor.constraint(equalTo: buttonContainerView.leftAnchor),

            buttonContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            buttonContainerView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            buttonContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.1),
            buttonContainerView.heightAnchor.constraint(equalTo: view.heightAnchor),
            
            lapButtonContainerView.topAnchor.constraint(equalTo: buttonContainerView.centerYAnchor),
            lapButtonContainerView.bottomAnchor.constraint(equalTo: buttonContainerView.bottomAnchor),
            lapButtonContainerView.leftAnchor.constraint(equalTo: buttonContainerView.leftAnchor),
            lapButtonContainerView.rightAnchor.constraint(equalTo: buttonContainerView.rightAnchor),
            
            resetButtonContainerView.topAnchor.constraint(equalTo: buttonContainerView.topAnchor),
            resetButtonContainerView.bottomAnchor.constraint(equalTo: buttonContainerView.centerYAnchor),
            resetButtonContainerView.leftAnchor.constraint(equalTo: buttonContainerView.leftAnchor),
            resetButtonContainerView.rightAnchor.constraint(equalTo: buttonContainerView.rightAnchor),
            
            timeButton.centerXAnchor.constraint(equalTo: timeContainerView.centerXAnchor),
            timeButton.centerYAnchor.constraint(equalTo: timeContainerView.centerYAnchor),
            timeButton.widthAnchor.constraint(equalTo: timeContainerView.widthAnchor, multiplier: 0.75),
            timeButton.heightAnchor.constraint(equalTo: timeButton.widthAnchor),
            
            lapButton.centerXAnchor.constraint(equalTo: lapButtonContainerView.centerXAnchor),
            lapButton.centerYAnchor.constraint(equalTo: lapButtonContainerView.centerYAnchor),
            lapButton.widthAnchor.constraint(equalTo: lapButtonContainerView.widthAnchor, multiplier: 0.7),
            lapButton.heightAnchor.constraint(equalTo: lapButtonContainerView.heightAnchor, multiplier: 0.6),
            
            resetButton.centerXAnchor.constraint(equalTo: resetButtonContainerView.centerXAnchor),
            resetButton.centerYAnchor.constraint(equalTo: resetButtonContainerView.centerYAnchor),
            resetButton.widthAnchor.constraint(equalTo: resetButtonContainerView.widthAnchor, multiplier: 0.7),
            resetButton.heightAnchor.constraint(equalTo: resetButtonContainerView.heightAnchor, multiplier: 0.6),
        ]
        
        sharedConstraints = [
            timeLabel.centerXAnchor.constraint(equalTo: timeButton.centerXAnchor),
            timeLabel.centerYAnchor.constraint(equalTo: timeButton.centerYAnchor),
            timeLabel.widthAnchor.constraint(equalTo: timeButton.widthAnchor, multiplier: 0.75),
            timeLabel.heightAnchor.constraint(equalTo: timeButton.heightAnchor, multiplier: 0.2),
            
            lapTableView.topAnchor.constraint(equalTo: lapContainerView.topAnchor),
            lapTableView.bottomAnchor.constraint(equalTo: lapContainerView.bottomAnchor),
            lapTableView.leftAnchor.constraint(equalTo: lapContainerView.leftAnchor),
            lapTableView.rightAnchor.constraint(equalTo: lapContainerView.rightAnchor),
        ]
    }
}

// MARK: - Table view handling
extension StopWatchViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return viewModel.tableView(tableView, viewForHeaderInSection: section)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.tableView(tableView, numberOfRowsInSection: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return viewModel.tableView(tableView, cellForRowAt: indexPath)
    }
}

// MARK: - Size class handling
extension StopWatchViewController {
    func layoutTrait(traitCollection: UITraitCollection) {
        if traitCollection.horizontalSizeClass == .compact, traitCollection.verticalSizeClass == .regular {
            if !regularConstraints.isEmpty, regularConstraints[0].isActive {
                NSLayoutConstraint.deactivate(regularConstraints)
            }
            NSLayoutConstraint.activate(compactConstraints)
        }
        else {
            if !compactConstraints.isEmpty, compactConstraints[0].isActive {
                NSLayoutConstraint.deactivate(compactConstraints)
            }
            NSLayoutConstraint.activate(regularConstraints)
        }
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        if (traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection)) {
            updateLayers()
        }
    }
    
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        super.willTransition(to: newCollection, with: coordinator)
        layoutTrait(traitCollection: newCollection)
    }
}
