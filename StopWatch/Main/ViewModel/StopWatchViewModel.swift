//
//  StopWatchViewModel.swift
//  StopWatch
//
//  Created by Jacob Bernharth on 2020-05-27.
//  Copyright © 2020 Jernharth. All rights reserved.
//

import Foundation
import UIKit

final class StopWatchViewModel {
    private let dataModel: StopWatchDataModelProtocol
    
    private let dateFormatter: DateFormatter
    
    private var displayLink: CADisplayLink?
    
    private(set) var isStopWatchRunning: Bool = false
    
    typealias TimeUpdated = (String) -> Void
    private var timeUpdated: TimeUpdated?
    
    init(dataModel: StopWatchDataModelProtocol) {
        self.dataModel = dataModel
        
        self.dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss.SS"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
    }
    
    func isElapsedValueZero() -> Bool {
        return dataModel.elapsedTime == 0
    }
    
    func getFormattedElapsedTime() -> String {
        let date = Date(timeIntervalSinceReferenceDate: dataModel.elapsedTime)
        return dateFormatter.string(from: date)
    }
}

// MARK: - Action handling
extension StopWatchViewModel {
    func createLap() {
        let date = Date(timeIntervalSinceReferenceDate: dataModel.elapsedTime)
        let time = dateFormatter.string(from: date)
        guard let previousLap = dataModel.laps.last, let previousDate = dateFormatter.date(from: previousLap.time) else {
            dataModel.updateLaps(with: Lap(time: time, difference: nil))
            return
        }
        let timeDifference = date.timeIntervalSinceReferenceDate - previousDate.timeIntervalSinceReferenceDate
        let timeDifferenceDate = Date(timeIntervalSinceReferenceDate: timeDifference)
        let difference = dateFormatter.string(from: timeDifferenceDate)
        dataModel.updateLaps(with: Lap(time: time, difference: difference))
    }
    
    func resetStopWatch() {
        dataModel.resetData()
    }
    
    func storeCurrentData() {
        dataModel.storeCurrentData()
    }
}

// MARK: - Time handling
extension StopWatchViewModel {
    func startReceivingTime(timeUpdated: @escaping TimeUpdated) {
        if displayLink == nil {
            displayLink = CADisplayLink(target: self, selector: #selector(tick))
            displayLink?.preferredFramesPerSecond = 60
            displayLink?.add(to: .current, forMode: .default)
        }
        displayLink?.isPaused = false
        isStopWatchRunning = true
        self.timeUpdated = timeUpdated
    }
    
    func pauseReceivingTime() {
        displayLink?.isPaused = true
        isStopWatchRunning = false
    }

    func stopReceivingTime() {
        displayLink?.invalidate()
        displayLink = nil
        dataModel.updateElapsedTime(with: -dataModel.elapsedTime)
        isStopWatchRunning = false
    }

    @objc private func tick() {
        dataModel.updateElapsedTime(with: displayLink?.duration ?? 0)
        timeUpdated?(getFormattedElapsedTime())
    }
}

// MARK: - Table view handling
extension StopWatchViewModel {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: LapTableHeaderView.reuseIdentifier) as? LapTableHeaderView else {
            return LapTableHeaderView()
        }
        return view
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataModel.laps.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: LapTableViewCell.reuseIdentifier, for: indexPath) as? LapTableViewCell else {
            return LapTableViewCell()
        }
        cell.configue(with: dataModel.laps[indexPath.row])
        return cell
    }
}
